dgedit (0.10.0-2) unstable; urgency=medium

  * Team upload

  [ Olivier Humbert ]
  * Update control (http->https)
  * Update copyright (http->https + empty lines)
  * Update dgedit.1 (http->https)
  * Add FR manpage
  * Update dgedit.manpages
  * Update copyright (add myself)

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1

  [ Víctor Cuadrado Juan ]
  * Bump Std-Ver to 4.5.0, no changes needed
  * Add Rules-Requires-Root: no
  * Add d/upstream/metadata file
  * Remove d/source/include-binaries, not needed anymore
  * Check upstream signature in d/watch

  [ Dennis Braun ]
  * Add pkg-config to B-Ds (Closes: #1020034)
  * Bump Standards Version to 4.6.1
  * Bump dh-compat to 13
  * Add salsa ci config

 -- Dennis Braun <d_braun@kabelmail.de>  Sat, 15 Oct 2022 20:53:05 +0200

dgedit (0.10.0-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Sebastian Ramacher ]
  * debian/watch: Add watch file.
  * New upstream release
    - Build with Qt 5. (Closes: #874852)
  * debian/: Remove get-orig-source.
  * debian/contol:
    - Bump debhelper compat to 12.
    - Bump Standards-Version.
  * debian/patches: Fix rcc invocation.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 24 Aug 2019 11:14:30 +0200

dgedit (0~git20160401-1) unstable; urgency=medium

  * New upstream release
     - New .desktop and icon
  * Add localized fr, es comments to .deskop (Closes: #819197)

 -- Víctor Cuadrado Juan <me@viccuad.me>  Fri, 25 Mar 2016 14:46:19 +0100

dgedit (0~git20151217-2) unstable; urgency=medium

  * Fix unsecure Vcs-Git URL
  * Add Suggests: Drumgizmo, now that it is packaged
  * Fix buildfags.mk usage, this removes lintian warnings
  * Update d/copyright to 2016
  * Update Std-Ver to 3.9.7

 -- Víctor Cuadrado Juan <me@viccuad.me>  Mon, 21 Mar 2016 14:34:37 +0100

dgedit (0~git20151217-1) unstable; urgency=low

  * New upstream snapshot, from git commit:
        9af1d8cec35d09b00a7e453b4632058dc48835ec
  * Fix reproducibility issue at tools/MocList. The fix was sent to
    upstream and is contained in this new snapshot.

 -- Víctor Cuadrado Juan <me@viccuad.me>  Thu, 17 Dec 2015 15:15:21 +0100

dgedit (0~git20151105-1) unstable; urgency=low

  * Initial release (Closes: #803400)

 -- Víctor Cuadrado Juan <me@viccuad.me>  Thu, 29 Oct 2015 21:16:09 +0100
