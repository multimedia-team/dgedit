<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.cc" line="43"/>
        <source>About DGEdit</source>
        <translation>À propos de DGEdit</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cc" line="81"/>
        <source>&lt;h1&gt;DGEdit&lt;/h1&gt;&lt;h2&gt;v.</source>
        <translation>&lt;h1&gt;DGEdit&lt;/h1&gt;&lt;h2&gt;v.</translation>
    </message>
</context>
<context>
    <name>CanvasToolListen</name>
    <message>
        <location filename="../canvastoollisten.h" line="41"/>
        <source>Listen</source>
        <translation>Écoute</translation>
    </message>
</context>
<context>
    <name>CanvasToolSelections</name>
    <message>
        <location filename="../canvastoolselections.h" line="45"/>
        <source>Selections</source>
        <translation>Sélections</translation>
    </message>
</context>
<context>
    <name>CanvasToolThreshold</name>
    <message>
        <location filename="../canvastoolthreshold.h" line="44"/>
        <source>Threshold</source>
        <translation>Seuil de déclenchement</translation>
    </message>
</context>
<context>
    <name>ChannelDialog</name>
    <message>
        <location filename="../channeldialog.cc" line="45"/>
        <source>Channel Dialog</source>
        <translation>Dialogue de canal</translation>
    </message>
    <message>
        <location filename="../channeldialog.cc" line="55"/>
        <source>Name of the channel:</source>
        <translation>Nom du canal : </translation>
    </message>
</context>
<context>
    <name>ChannelMapDeligate</name>
    <message>
        <location filename="../filelist.cc" line="104"/>
        <source>&lt;None&gt;</source>
        <translation>&lt;None&gt;</translation>
    </message>
</context>
<context>
    <name>FileDataModel</name>
    <message>
        <location filename="../filelist.cc" line="309"/>
        <source>&lt;None&gt;</source>
        <translation>&lt;Aucun&gt;</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="341"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="342"/>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="343"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="344"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="345"/>
        <source>Kit Channel</source>
        <translation>Canal de kit</translation>
    </message>
</context>
<context>
    <name>FileList</name>
    <message>
        <location filename="../filelist.cc" line="462"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="463"/>
        <source>Audio Files (*.wav)</source>
        <translation>Fichiers audio (*.wav)</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="530"/>
        <source>Set as Master (dbl-click)</source>
        <translation>Paramétrer en tant que maître (dbl-clic)</translation>
    </message>
    <message>
        <source>Edit name</source>
        <translation type="vanished">Éditer le nom</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="533"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="536"/>
        <source>Remove all</source>
        <translation>Supprimer tout</translation>
    </message>
</context>
<context>
    <name>InstrumentDialog</name>
    <message>
        <location filename="../instrumentdialog.cc" line="45"/>
        <source>Instrument Dialog</source>
        <translation>Dialogue d&apos;instrument</translation>
    </message>
    <message>
        <location filename="../instrumentdialog.cc" line="55"/>
        <source>Name of the instrument:</source>
        <translation>Nom de l&apos;instrument : </translation>
    </message>
</context>
<context>
    <name>InstrumentWidget</name>
    <message>
        <location filename="../instrumentwidget.cc" line="92"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="123"/>
        <source>Play samples</source>
        <translation>Lire les échantillons</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="137"/>
        <source>Process</source>
        <translation>Traiter</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="146"/>
        <source>Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="147"/>
        <location filename="../instrumentwidget.cc" line="272"/>
        <source>Generate</source>
        <translation>Générer</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="148"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="193"/>
        <source>Files: (double-click to set as master)</source>
        <translation>Fichiers : (double-clic pour paramétrer en tant que maître)</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="195"/>
        <source>Add files...</source>
        <translation>Ajouter des fichiers...</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="284"/>
        <source>Clear</source>
        <translation>Vider</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="293"/>
        <source>Attack length:</source>
        <translation>Longueur d&apos;attaque :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="302"/>
        <source>Power spread:</source>
        <translation>Étalement de la puissance :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="311"/>
        <source>Minimum size (samples):</source>
        <translation>Taille minimum (échantillons) :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="320"/>
        <source>Falloff:</source>
        <translation>Temps de chute :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="329"/>
        <source>Fadelength:</source>
        <translation>Longueur de fondu :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="359"/>
        <source>Prefix:</source>
        <translation>Préfixe :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="366"/>
        <source>Export path:</source>
        <translation>Chemin d&apos;exportation :</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="373"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="380"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="475"/>
        <source>Loading...</source>
        <translation>Chargement...</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="486"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="495"/>
        <source>Select export path</source>
        <translation>Sélectionner le chemin d&apos;exportation</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tools</source>
        <translation type="vanished">Outils</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="67"/>
        <source>&amp;File</source>
        <translation>&amp;Fichiers</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="69"/>
        <source>&amp;New Project</source>
        <translation>&amp;Nouveau projet</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="73"/>
        <source>&amp;Load Project...</source>
        <translation>&amp;Charger un projet...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="77"/>
        <source>&amp;Save Project</source>
        <translation>&amp;Sauvegarder le projet</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="82"/>
        <source>Save Project As...</source>
        <translation>Sauvegarder le projet en tant que...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="87"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="91"/>
        <source>&amp;Project</source>
        <translation>&amp;Projet</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="92"/>
        <source>&amp;Export...</source>
        <translation>&amp;Exporter...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="96"/>
        <source>&amp;Edit Project...</source>
        <translation>&amp;Éditer le projet...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="105"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="106"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="110"/>
        <source>Instruments:</source>
        <translation>Instruments :</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="142"/>
        <source>Kit channels:</source>
        <translation>Canaux de kit :</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="229"/>
        <source>Delete Instrument</source>
        <translation>Effacer l&apos;instrument</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="230"/>
        <source>Are you sure you want to delete the selected instrument(s)?</source>
        <translation>Êtes-vous sûr de vouloir effacer l&apos;(les)instrument(s) sélectionné(s) ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="298"/>
        <source>Delete Channel</source>
        <translation>Effacer le canal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="299"/>
        <source>Are you sure you want to delete the selected channel(s)?</source>
        <translation>Êtes-vous sûr de vouloir effacer le ou les canaux sélectionnés ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="337"/>
        <source>Channels</source>
        <translation>Canaux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="348"/>
        <source>[Untitled Project]</source>
        <translation>[projet sans nom]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="383"/>
        <source>Save before closing project?</source>
        <translation>Sauvegarder avant de fermer le projet ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="384"/>
        <source>The project has changed. Do you want to save before closing?</source>
        <translation>Le projet a été modifié. Voulez-vous sauvegarder avant de fermer ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="473"/>
        <source>Load DGEdit Project</source>
        <translation>Charger un projet DGEdit</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="475"/>
        <location filename="../mainwindow.cc" line="566"/>
        <source>DGEdit Project Files (*.dgedit)</source>
        <translation>Fichiers de projet DGEdit (*.dgedit)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="530"/>
        <source>Loaded</source>
        <translation>Chargé</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="558"/>
        <source>Saved</source>
        <translation>Sauvegardé</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="564"/>
        <source>Save DGEdit Project</source>
        <translation>Sauvegarder un projet DGEdit</translation>
    </message>
    <message>
        <source>Play samples</source>
        <translation type="vanished">Lire les échantillons</translation>
    </message>
    <message>
        <source>Dock Widget</source>
        <translation type="vanished">Quai des gadgets</translation>
    </message>
    <message>
        <source>Files</source>
        <translation type="vanished">Fichiers</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Éditer</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Exporter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="171"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>Files: (double-click to set as master)</source>
        <translation type="vanished">Fichiers : (double-clic pour paramétrer en tant que maître)</translation>
    </message>
    <message>
        <source>Add files...</source>
        <translation type="vanished">Ajouter des fichiers....</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation type="vanished">Générer</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Vider</translation>
    </message>
    <message>
        <source>Attack length:</source>
        <translation type="vanished">Longueur d&apos;attaque :</translation>
    </message>
    <message>
        <source>Power spread:</source>
        <translation type="vanished">Étalement de la puissance :</translation>
    </message>
    <message>
        <source>Minimum size (samples):</source>
        <translation type="vanished">Taille minimum (échantillons) :</translation>
    </message>
    <message>
        <source>Falloff:</source>
        <translation type="vanished">Temps de chute :</translation>
    </message>
    <message>
        <source>Fadelength:</source>
        <translation type="vanished">Longueur de fondu :</translation>
    </message>
    <message>
        <source>Prefix:</source>
        <translation type="vanished">Préfixe :</translation>
    </message>
    <message>
        <source>Export path:</source>
        <translation type="vanished">Chemin d&apos;exportation :</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Chargement...</translation>
    </message>
    <message>
        <source>Select export path</source>
        <translation type="vanished">Sélectionner le chemin d&apos;exportation</translation>
    </message>
</context>
<context>
    <name>ProjectDialog</name>
    <message>
        <location filename="../projectdialog.cc" line="46"/>
        <source>Project Dialog</source>
        <translation>Dialogue de projet</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="56"/>
        <source>Name of the project:</source>
        <translation>Nom du projet :</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="63"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="66"/>
        <source>Base path of raw files:</source>
        <translation>Chemin de base des fichiers bruts (raw)</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="74"/>
        <source>Description of the project:</source>
        <translation>Description du projet :</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="95"/>
        <source>Choose base directory with the raw sample files.</source>
        <translation>Choisir un répertoire de base avec des fichiers d&apos;échantillons bruts.</translation>
    </message>
</context>
<context>
    <name>ProjectRenderer</name>
    <message>
        <location filename="../projectrenderer.cc" line="84"/>
        <source>Writing instrument: </source>
        <translation>Écrire l&apos;instrument : </translation>
    </message>
</context>
<context>
    <name>RenderDialog</name>
    <message>
        <location filename="../renderdialog.cc" line="280"/>
        <source>Export drumkit</source>
        <translation>Exporter le kit de batterie</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="286"/>
        <source>Export path:</source>
        <translation>Chemin d&apos;exportation :</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="294"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="303"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="405"/>
        <source>Choose export directory.</source>
        <translation>Choisir le répertoire d&apos;exportation.</translation>
    </message>
</context>
<context>
    <name>SelectionEditor</name>
    <message>
        <location filename="../selectioneditor.cc" line="60"/>
        <source>From:</source>
        <translation>Depuis :</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="61"/>
        <source>To:</source>
        <translation>Jusqu&apos;à :</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="62"/>
        <source>FadeIn:</source>
        <translation>Fondu ouverture :</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="63"/>
        <source>FadeOut:</source>
        <translation>Fondu fermeture :</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="64"/>
        <source>Energy:</source>
        <translation>Énergie :</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="65"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
</context>
<context>
    <name>VolumeFader</name>
    <message>
        <location filename="../volumefader.cc" line="73"/>
        <source>Peak </source>
        <translation>Pic </translation>
    </message>
    <message>
        <location filename="../volumefader.cc" line="97"/>
        <source>Gain %1 dB</source>
        <translation>Gain %1 dB</translation>
    </message>
</context>
</TS>

