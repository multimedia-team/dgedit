/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            canvaswidget.h
 *
 *  Fri Aug  1 19:31:31 CEST 2014
 *  Copyright 2014 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <QWidget>

#include <QScrollBar>

#include "canvas.h"
#include "zoomslider.h"

class CanvasWidget
	: public QWidget
{
	Q_OBJECT
public:
	CanvasWidget(QWidget* parent);

	Canvas* canvas;

	QScrollBar* yoffset;
	ZoomSlider* yscale;
	ZoomSlider* xscale;
	QScrollBar* xoffset;

public slots:
	void setXScale(float);
	void setYScale(float);
	void setXOffset(int);
	void setYOffset(int);
};
