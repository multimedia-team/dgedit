/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            filelist.h
 *
 *  Mon Nov 30 15:35:52 CET 2009
 *  Copyright 2009 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <QTreeView>
#include <QAction>
#include <QMenu>

class Instrument;
class FileDataModel;

class FileList
	: public QTreeView
{
	Q_OBJECT
public:
	FileList(Instrument& instrument);

	QString path;

signals:
	void masterFileChanged(QString filename);
//	void fileAdded(QString file, QString name);
	void fileRemoved(QString file, QString name);
	void allFilesRemoved();
//	void nameChanged(QString file, QString name);

public slots:
	void addFiles();

private slots:
	void selectionChanged(const QModelIndex &index);
	void onCustomContextMenu(const QPoint &point);
	void setMaster();
	void removeFile();
	void removeAllFiles();
//	void setItemName(QListWidgetItem* i, QString name);
//
private:
//	QString itemFile(QListWidgetItem* i);
//	void setItemFile(QListWidgetItem* i, QString file);
//
//	QString itemName(QListWidgetItem* i);
//	void setItemMaster(QListWidgetItem* i, bool master);
//
//	int itemChannelMap(QListWidgetItem* i);
//	void setItemChannelMap(QListWidgetItem* i, int id);
//
//	void setMasterFile(QListWidgetItem* i);
	void createMenus();

	QMenu* menu;
	QAction* setMasterAction;
	QAction* editAction;
	QAction* removeAction;
	QAction* removeAllAction;

//	QListWidgetItem* activeItem;
	Instrument& instrument;
	FileDataModel* model;
};
