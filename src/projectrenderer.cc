/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            projectrender.cc
 *
 *  Sat Sep 22 10:35:01 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "projectrenderer.h"

#include <QDomDocument>
#include <QFile>
#include <QApplication>
#include <QDir>
#include <QHash>

#include "project.h"
#include "audioextractor.h"

ProjectRenderer::ProjectRenderer(Project& project)
	: project(project)
{
}

void ProjectRenderer::render()
{
	// Ugly hack to ensure the xml attribute order is the same each time a save
	// or export is performed.
	qSetGlobalQHashSeed(0);

	QDomDocument doc;
	QDomProcessingInstruction header =
		doc.createProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
	doc.appendChild(header);

	QDomElement drumkit = doc.createElement("drumkit");
	drumkit.setAttribute("version", "1.0");
	drumkit.setAttribute("name", project.getProjectName());
	drumkit.setAttribute("description", project.getProjectDescription());
	drumkit.setAttribute("samplerate", project.getProjectSamplerate());
	doc.appendChild(drumkit);

	QDomElement channels = doc.createElement("channels");
	drumkit.appendChild(channels);

	auto channel_ids = project.getChannelList();
	for(auto channel_id : channel_ids)
	{
		const auto& channel = project.getChannel(channel_id);
		QDomElement channel_node = doc.createElement("channel");
		channel_node.setAttribute("name", channel.getChannelName());
		channels.appendChild(channel_node);
	}

	QDomElement instruments = doc.createElement("instruments");
	drumkit.appendChild(instruments);

	auto instrument_ids = project.getInstrumentList();

	emit progressStart(instrument_ids.count());
	qApp->processEvents();

	for(auto instrument_id : instrument_ids)
	{
		auto& instrument = project.getInstrument(instrument_id);
		emit progressTask(tr("Writing instrument: ") +
		                  instrument.getInstrumentName());

		{
			auto selections = instrument.getSelections().ids();
			emit progressRenderStart(selections.count());
			qApp->processEvents();
		}

		AudioExtractor extractor(instrument, this);
		connect(&extractor, SIGNAL(progressUpdate(int)),
		        this, SIGNAL(progressRenderTask(int)));

		if(!extractor.exportSelections())
		{
			emit progressFinished(1);
			qApp->processEvents();
			return;
		}

		QDomElement instrument_node = doc.createElement("instrument");
		instrument_node.setAttribute("name", instrument.getInstrumentName());
		QString file = instrument.getPrefix() + QDir::separator() + instrument.getPrefix() + ".xml";
		instrument_node.setAttribute("file", file);
		instruments.appendChild(instrument_node);
		auto audiofile_ids = instrument.getAudioFileList();
		for(auto audiofile_id : audiofile_ids)
		{
			const auto& audiofile = instrument.getAudioFile(audiofile_id);
			if(audiofile.getChannelMapId() == -1)
			{
				// Not mapped
				continue;
			}
			QDomElement channelmap = doc.createElement("channelmap");
			channelmap.setAttribute("in", audiofile.getName());
			const auto& channel = project.getChannel(audiofile.getChannelMapId());
			channelmap.setAttribute("out", channel.getChannelName());
			if(audiofile.getMainChannel())
			{
				channelmap.setAttribute("main", "true");
			}
			instrument_node.appendChild(channelmap);
		}

		emit progressRenderFinished(0);
		qApp->processEvents();
	}

	QFile xmlfile(project.getExportPath() + QDir::separator() + "drumkit.xml");
	if(!xmlfile.open(QIODevice::WriteOnly))
	{
		emit progressFinished(1);
		qApp->processEvents();
		return;
	}
	xmlfile.write(doc.toByteArray());
	xmlfile.close();

	emit progressFinished(0);
	qApp->processEvents();
}
