/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            mainwindow.h
 *
 *  Tue Nov 10 10:21:03 CET 2009
 *  Copyright 2009 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <QMainWindow>
#include <QCloseEvent>
#include <QDockWidget>
#include <QTabWidget>
#include <QListWidget>

#include "project.h"

class Settings;

class MainWindow
	: public QMainWindow
{
	Q_OBJECT
public:
	MainWindow(Settings& settings);
	~MainWindow();

public slots:
	void addInstrument();
	void editInstrument();
	void removeInstrument();
	void instrumentDoubleClicked(QListWidgetItem *item);

	void addChannel();
	void removeChannel();
	void channelDoubleClicked(QListWidgetItem *item);

	void updateWindowTitle();

	void newProject();
	void loadProject();
	void loadProject(QString filename);
	void saveProject();
	void saveProjectAs();
	void projectChanged();

	void closeTab(int tab);

private slots:
	void test();
	void render();
	void editProject();
	void showAbout();

protected:
	void closeEvent(QCloseEvent*);

private:
	//! Check dirty and prompt user to save
	//! \returns true to continue closing the project, false to bail out.
	bool checkDirty();

	//! Reset project and reflect in mainwindow (close tabs and lists)
	void reset();

	//! Controls wether the window components are enabled for the user to use.
	void setWindowEnabled(bool enabled);

	void loadSettings();
	void saveSettings();

	Settings& settings;
	Project project;
	bool project_dirty{false};

	QDockWidget* instruments_dock;
	QListWidget* instrument_list;
	QDockWidget* channels_dock;
	QListWidget* channel_list;
	QTabWidget* tab_widget;

	QAction* act_save_project;
};
