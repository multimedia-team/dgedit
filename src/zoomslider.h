/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            zoomslider.h
 *
 *  Fri May  2 21:23:26 CEST 2014
 *  Copyright 2014 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <QSlider>
#include <QObject>

class ZoomSlider
	: public QSlider
{
	Q_OBJECT
public:
	ZoomSlider(Qt::Orientation orientation);

	//! Range of the slider. 'from' may be lesser than 'to' which will invert the
	//! slider.
	void setRange(float from, float to);

	//! Set the distance between emitting of valueChanged signals.
	//! Example: from:=0, to:=1 and width:=0.5 will emit valueChanged signals on
	//! 0.0, 0.5 and 1.0 (ie. the slider will have 3 actual values)
	//! NOTE: If 'width' is greater than the span of [from; to] only the 'from'
	//! and 'to' values can be selected on the slider.
	void setTickWidth(float width);

	void setValue(float value);

signals:
	void valueChanged(float value);

private slots:
	void sliderValueChanged(int value);

protected:
	void paintEvent(QPaintEvent* ev);

private:
	float fromSlider(int value);

	bool inverted;
	float from;
	float to;
	float tick_width;
};
