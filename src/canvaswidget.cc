/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            canvaswidget.cc
 *
 *  Fri Aug  1 19:31:32 CEST 2014
 *  Copyright 2014 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "canvaswidget.h"

#include <QGridLayout>

#define MAXVAL 10000000L
#define SINGLESTEP MAXVAL/100000
#define PAGESTEP MAXVAL/10000

CanvasWidget::CanvasWidget(QWidget* parent)
{
	QGridLayout* g = new QGridLayout(this);

	canvas = new Canvas(this);
	g->addWidget(canvas, 0, 0);

	yoffset = new QScrollBar(Qt::Vertical);
	yoffset->setRange(0, MAXVAL);
	yoffset->setPageStep(PAGESTEP);
	yoffset->setSingleStep(SINGLESTEP);
	connect(yoffset, SIGNAL(valueChanged(int)), this, SLOT(setYOffset(int)));
	g->addWidget(yoffset, 0, 1);

	yscale = new ZoomSlider(Qt::Vertical);
	yscale->setRange(0.5, 30);
	yscale->setTickWidth(0.1);
	connect(yscale, SIGNAL(valueChanged(float)), this, SLOT(setYScale(float)));
	yscale->setValue(0.5);
	setYScale(0.5);
	g->addWidget(yscale, 0, 2);

	xoffset = new QScrollBar(Qt::Horizontal);
	xoffset->setRange(0, MAXVAL);
	xoffset->setPageStep(PAGESTEP);
	xoffset->setSingleStep(SINGLESTEP);
	connect(xoffset, SIGNAL(valueChanged(int)), this, SLOT(setXOffset(int)));
	g->addWidget(xoffset, 1, 0);

	xscale = new ZoomSlider(Qt::Horizontal);
	xscale->setRange(1, 0.0001);
	xscale->setTickWidth(0.0001);
	connect(xscale, SIGNAL(valueChanged(float)), this, SLOT(setXScale(float)));
	g->addWidget(xscale, 2, 0);

	setLayout(g);
}

void CanvasWidget::setXScale(float val)
{
	canvas->setXScale(val);
}

void CanvasWidget::setYScale(float val)
{
	canvas->setYScale(val);
}

void CanvasWidget::setXOffset(int of)
{
	// range 0.0 - 1.0
	float val = (float)of/(float)MAXVAL;
	canvas->setXOffset(val);
}

void CanvasWidget::setYOffset(int of)
{
	// range 0.0 - 1.0
	float val = (float)(of * -1 + MAXVAL)/(float)MAXVAL;
	canvas->setYOffset(val);
}
